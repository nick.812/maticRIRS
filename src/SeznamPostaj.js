import React from 'react';
import './styling.css';

export default function SeznamPostaj() {
  const Fruits = [
    { name: 'Europark' },
    { name: 'Ledna dvorana' },
    { name: 'Honey berry' },
    { name: 'Papaya' },
    { name: 'Jambul' },
    { name: 'Plum' },
    { name: 'Lemon' },
    { name: 'Pomelo' }
  ];
  return (
    <div>
      <div class="header">
  <a href="#default" class="logo">Seznam Postaj</a>
  <div class="header-right">
    <a class="active" href="#home">Nazaj</a>
  </div>
</div>
       <ul class="list-group">
      {Fruits.map(data => (
        <li class="list-group-item">{data.name}</li>
      ))}</ul>
    </div>
  );
}
