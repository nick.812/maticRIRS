import React from 'react'
import { useNavigate } from 'react-router-dom';
import { useState } from "react";

export default function Login() {

    //const navigate = useNavigate();

    const [email, setEmail] = useState(''); 
    const [password, setPassword] = useState('');

    function checklogin(em, pw){
        const emails = ["test","nik","tristan","matic"]
        const passwords = ["test","jeromel","hudales","m"]

        if(emails.includes(em)){
            if(passwords[emails.indexOf(em)]==pw){
                return true
            }
        }
        return false
    }

    function handleSubmit(){
        if (checklogin(email,password))
        {
            //navigate('/dodajanjepostaj');
            console.log("successfulLogin");

            
        }
        else
        {
            console.log("failedLogin");
        }
    }
    
    return (
        <>
            <div style={{
                backgroundColor: '#E72F39',
                height: '80px',
                margin: 2,
                borderRadius: '100px',
                textAlign: 'right',
            }}>
                <p style={{
                    backgroundColor: "transparent",
                    fontSize: '30px',
                    color: 'white',
                    lineHeight:'100%',
                    fontFamily: 'Trebuchet MS, sans-serif',
                    fontWeight: 'bold',
                    paddingRight: 40,
                    paddingTop: 25
                    }}>Login
                </p>
            </div>
            <div style={{
                textAlign: 'center',
            }}>
                <div style={{
                    backgroundColor: 'white',
                    width: '30%',
                    margin: '0 auto',
                    padding: 10,
                    position: 'relative',
                    borderStyle:'solid',
                    borderColor: '#707070',
                    borderWidth: '1.5px',
                    borderRadius: '5px',
                    marginTop: 30,
                    paddingTop: 0
                }}>
                    <h3 style={{
                        color: '#2D2D2C',
                        fontSize: '22px',
                        fontFamily: 'Trebuchet MS, sans-serif',
                        fontWeight: 'bold'
                    }}>
                        Email:
                    </h3>
                    <input id="emailInput" type="email" onChange={event => setEmail(event.target.value)} style={{
                        color: '#2D2D2C',
                        fontSize: '20px',
                        padding: '1%',
                        width:'80%',
                        fontFamily: 'Trebuchet MS, sans-serif',
                        fontWeight: 'bold',
                        borderRadius: '5px',
                        borderStyle:'solid',
                        borderColor: '#707070',
                        borderWidth: '1.5px'
                    }}/>
                    <h3 style={{
                        color: '#2D2D2C',
                        fontSize: '22px',
                        fontFamily: 'Trebuchet MS, sans-serif',
                        fontWeight: 'bold'
                    }}>
                        Password:
                    </h3>
                    <input id="passInput" type="password" onChange={event => setPassword(event.target.value)} style={{
                        color: '#2D2D2C',
                        fontSize: '20px',
                        padding: '1%',
                        width:'80%',
                        fontFamily: 'Trebuchet MS, sans-serif',
                        fontWeight: 'bold',
                        borderRadius: '5px',
                        borderStyle:'solid',
                        borderColor: '#707070',
                        borderWidth: '1.5px'
                    }}/>
                    <br/>
                    <button id="buttonLogins" onClick={handleSubmit} style={{
                            fontSize: '20px',
                            padding: '2%',
                            width: '83%',
                            backgroundColor: '#E72F39',
                            color: "white",
                            borderRadius: '10px',
                            borderColor: '#E72F39',
                            borderStyle:'solid',
                            marginTop: '20px'
                        }}>
                        Logins
                    </button>
                </div>
            </div>    
        </>
    )
}
