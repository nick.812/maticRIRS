import React, { useState } from 'react';



export default function DodajanjeKoles() {

  const [setEnteredText] = useState(''); 
  const [setEnteredPosta] = useState(''); 
  const [setEnteredStevilo] = useState(''); 
  
  const changeColor = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
  }

  const backColor = event => {
    event.currentTarget.style.backgroundColor = '#E72F39';
    event.currentTarget.style.borderColor = '#E72F39';
  }


  const click = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
    setEnteredText('');
    setEnteredStevilo('');
    setEnteredPosta('');
  }


  const [ID, setID] = useState(''); 

  function handleSubmit(){
    let input = document.getElementById("inputIme");
    console.log("Dodano kolo.");
    input.value = "";
}
  
  return (
    
    <div 
      style={{
        backgroundColor: 'white',
        height: '800px'
      }}>
      <div
        style={{
          backgroundColor: 'white',
          width: '10%',
          height: '10%',
          float: 'left',
          marginLeft: '2%',
          marginTop: '2%',
          marginRight: '2%',
          display: 'inline-block'
        }}>
        <img id = "logo" src="https://www.mbajk.si/assets/img/logo-contract.png" alt="logo"
        style={{
          margin: 0,
          position: 'relative',
          top: '50%',
          left: '50%',
          height: '100%',
          transform: 'translate(-50%, -50%)'
        }}
        ></img>
      </div>
      <div 
      style={{
        backgroundColor: '#E72F39',
        display:'inline-block',
        marginTop: '2%',
        height: '10%',
        width: '84%',
        borderRadius: '100px'
      }}>
        <div 
        style={{
        display:'table',
        marginLeft: '2%',
        height: '100%'
      }}>
          <p
            style={{
              backgroundColor: "transparent",
              fontSize: '28px',
              margin: '0%',
              color: 'white',
              lineHeight:'100%',
              display: 'table-cell',
              verticalAlign: 'middle',
              fontFamily: 'Trebuchet MS, sans-serif',
              fontWeight: 'bold'
            }}
          >
            Dodaj novo kolo
          </p>
        </div>
      </div>
      <div 
      style={{
        display:'inline-block',
        marginLeft: '2%',
        marginTop: '2%',
        backgroundColor: 'white',
        height: '76%',
        width: '96%'
      }}>
      <h3
      style={{
        marginLeft: '3%',
        marginTop: '2%',
        color: '#2D2D2C',
        fontSize: '22px',
        fontFamily: 'Trebuchet MS, sans-serif',
        fontWeight: 'bold'
      }}>ID</h3>
      <input id = "inputIme" type="text" onChange={event => setID(event.target.value)}
        style={{
          marginLeft: '3%',
          color: '#2D2D2C',
          fontSize: '20px',
          padding: '0.4%',
          width:'30%',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold',
          borderRadius: '5px',
          borderStyle:'solid',
          borderColor: '#707070',
          borderWidth: '1.5px'
        }}
      />
      <h3
        style={{
          marginLeft: '3%',
          marginTop: '2%',
          color: '#2D2D2C',
          fontSize: '22px',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold'
        }}
      >Tip</h3>
      <select name="kolesa" style={{
          marginLeft: '3%',
          color: '#2D2D2C',
          fontSize: '20px',
          padding: '0.4%',
          width:'30%',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold',
          borderRadius: '5px',
          borderStyle:'solid',
          borderColor: '#707070',
          borderWidth: '1.5px'
        }}>
        <option value="1">Navadno-Veliko</option>
        <option value="2">Navadno-Malo</option>
        <option value="3">Električno-Veliko</option>
        <option value="4">Električno-Malo</option>
      </select>
      <br/>
      <button id="btn" onClick={handleSubmit}
        style={{
            marginLeft: '3%',
            marginTop: '2%',
            fontSize: '20px',
            padding: '0.4%',
            width: '31%',
            backgroundColor: '#E72F39',
            color: "white",
            borderRadius: '10px',
            borderColor: '#E72F39',
            borderStyle:'solid'
          }}
          onMouseEnter={changeColor}
          onMouseLeave={backColor}
          onMouseDown={backColor}
          onMouseUp={changeColor}
          onMouseUpCapture={click}
          >
        Dodaj
      </button>
      </div>
    </div>
  )
}
