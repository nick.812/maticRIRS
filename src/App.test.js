import { fireEvent, getElementById, render, screen } from '@testing-library/react';
import DodajanjePostaj from './DodajanjePostaj.js';
import DodajanjeKoles from './DodajanjeKoles.js';
import Mapa from './Mapa.js';
import Login from './Login.js';
import SeznamPostaj from './SeznamPostaj.js'
import "@testing-library/jest-dom"

let izhodData = "";
let storeLog = inputs => (izhodData += inputs);

test('Check Login page render.', () => {
  render(<Login />);
  const linkElement = screen.getByText(/Logins/i);
  expect(linkElement).toBeInTheDocument();
});

test('Check DodajanjePostaj page render.', () => {
  render(<DodajanjePostaj />);
  const linkElement = screen.getByText(/Dodaj novo postajo/i);
  expect(linkElement).toBeInTheDocument();
});

test('Check SeznamPostaj page render.', () => {
  render(<SeznamPostaj />);
  const linkElement = screen.getByText(/Seznam Postaj/i);
  expect(linkElement).toBeInTheDocument();
});

test('Check valid login.', () => {
  
  const result = render(<Login />);

  console["log"] = jest.fn(storeLog);

  const emailElement = result.container.querySelector('#emailInput');
  const passwordElement = result.container.querySelector('#passInput');
  const buttonElement = result.container.querySelector('#buttonLogins');

  fireEvent.change(emailElement, { target: { value: "test" } });
  fireEvent.change(passwordElement, { target: { value: "test" } });
  fireEvent.click(buttonElement);

  expect(izhodData).toBe("successfulLogin");
});

test('Check invalid login (wrong email).', () => {
  
  const result = render(<Login />);

  console["log"] = jest.fn(storeLog);

  const emailElement = result.container.querySelector('#emailInput');
  const passwordElement = result.container.querySelector('#passInput');
  const buttonElement = result.container.querySelector('#buttonLogins');

  fireEvent.change(emailElement, { target: { value: "napacno" } });
  fireEvent.change(passwordElement, { target: { value: "test" } });
  fireEvent.click(buttonElement);

  expect(izhodData.slice(-11)).toBe("failedLogin");
});

test('Check invalid login (wrong password).', () => {
  
  const result = render(<Login />);

  console["log"] = jest.fn(storeLog);

  const emailElement = result.container.querySelector('#emailInput');
  const passwordElement = result.container.querySelector('#passInput');
  const buttonElement = result.container.querySelector('#buttonLogins');

  fireEvent.change(emailElement, { target: { value: "test" } });
  fireEvent.change(passwordElement, { target: { value: "napacno" } });
  fireEvent.click(buttonElement);

  expect(izhodData.slice(-11)).toBe("failedLogin");
});

test('Check invalid login (wrong email, password).', () => {
  
  const result = render(<Login />);

  console["log"] = jest.fn(storeLog);

  const emailElement = result.container.querySelector('#emailInput');
  const passwordElement = result.container.querySelector('#passInput');
  const buttonElement = result.container.querySelector('#buttonLogins');

  fireEvent.change(emailElement, { target: { value: "napacno" } });
  fireEvent.change(passwordElement, { target: { value: "napacno" } });
  fireEvent.click(buttonElement);

  expect(izhodData.slice(-11)).toBe("failedLogin");
});

test('Check dodajanje postaj gumb.', () => {
  const result = render(<DodajanjePostaj />);

  const gumbElement = result.container.querySelector('#btn');
  const vnosElement = result.container.querySelector('#inputIme');
  fireEvent.change(vnosElement, { target: { value: "test" } });
  fireEvent.mouseUp(gumbElement);
  expect(vnosElement.value).toBe("");
});

test('Check image alt text.', () => {
  const result = render(<DodajanjePostaj />);
  const slikaElement = result.container.querySelector('#logo');
  expect(slikaElement.alt).toContain("logo");
});

test('Check image source.', () => {
  const result = render(<DodajanjePostaj />);
  const slikaElement = result.container.querySelector('#logo');
  expect(slikaElement.src).toContain("https://www.mbajk.si/assets/img/logo-contract.png");
});

test('Check dodajanje kolesa gumb.', () => {
  const result = render(<DodajanjeKoles />);

  const gumbElement = result.container.querySelector('#btn');
  fireEvent.mouseUp(gumbElement);
  const vnosElement = result.container.querySelector('#inputIme');
  expect(vnosElement.value).toBe("");
});

test('Check dodajanje kolesa konzola.', () => {
  const result = render(<DodajanjeKoles />);

  console["log"] = jest.fn(storeLog);

  const buttonElement = result.container.querySelector('#btn');

  fireEvent.click(buttonElement);

  expect(izhodData.slice(-12)).toBe("Dodano kolo.");
});

test('Check width karte.', () => {
  const result = render(<Mapa />);

  const karta = result.container.querySelector('#karta');

  expect(karta.attributes.getNamedItem("width").value).toBe("600px");
});

test('Check height karte.', () => {
  const result = render(<Mapa />);

  const karta = result.container.querySelector('#karta');

  expect(karta.attributes.getNamedItem("height").value).toBe("450px");
});
//