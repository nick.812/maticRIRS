import React from 'react';
import DodajanjePostaj from './DodajanjePostaj.js';
import DodajanjeKoles from './DodajanjeKoles.js';
import Mapa from './Mapa.js';
import Login from './Login.js';
import SeznamPostaj from './SeznamPostaj.js'
import { Routes, Route } from "react-router-dom";

document.body.style = 'background: whitesmoke;';

function App() {
  return (
    <div className='App'>
      <Routes><Route path="/" element={<Login />} /></Routes>
      <Routes><Route path="/login" element={<Login />} /></Routes>
      <Routes><Route path="/dodajanjepostaj" element={<DodajanjePostaj />} /></Routes>  
      <Routes><Route path="/seznampostaje" element={<SeznamPostaj />} /></Routes>
      <Routes><Route path="/dodajanjekoles" element={<DodajanjeKoles />} /></Routes>
      <Routes><Route path="/mapa" element={<Mapa />} /></Routes> 
    </div>
  )
}

export default App;
