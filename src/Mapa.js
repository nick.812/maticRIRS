import React from 'react'
import { useState } from "react";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;

export default function DodajanjeKoles() {

  const defaultProps = {
    center: {
      lat: 46.55903,
      lng: 15.63786
    },
    zoom: 15
  };

  const [enteredText, setEnteredText] = useState(''); 
  const [enteredPosta, setEnteredPosta] = useState(''); 
  const [enteredStevilo, setEnteredStevilo] = useState(''); 
  
  const changeColor = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
  }

  const backColor = event => {
    event.currentTarget.style.backgroundColor = '#E72F39';
    event.currentTarget.style.borderColor = '#E72F39';
  }


  const click = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
    setEnteredText('');
    setEnteredStevilo('');
    setEnteredPosta('');
  }

  const handle = ({target:{value}}) => setEnteredText(value)
  const handlePosta = ({target:{value}}) => setEnteredPosta(value)
  const handleStevilo = ({target:{value}}) => setEnteredStevilo(value)


  const [ID, setID] = useState(''); 

  function handleSubmit(){
    let input = document.getElementById("inputIme");
    console.log("Dodano kolo.");
    input.value = "";
}
  
  return (
    
    <div 
      style={{
        backgroundColor: 'white',
        height: '800px'
      }}>
      <div
        style={{
          backgroundColor: 'white',
          width: '10%',
          height: '10%',
          float: 'left',
          marginLeft: '2%',
          marginTop: '2%',
          marginRight: '2%',
          display: 'inline-block'
        }}>
        <img id = "logo" src="https://www.mbajk.si/assets/img/logo-contract.png" alt="logo"
        style={{
          margin: 0,
          position: 'relative',
          top: '50%',
          left: '50%',
          height: '100%',
          transform: 'translate(-50%, -50%)'
        }}
        ></img>
      </div>
      <div 
      style={{
        backgroundColor: '#E72F39',
        display:'inline-block',
        marginTop: '2%',
        height: '10%',
        width: '84%',
        borderRadius: '100px'
      }}>
        <div 
        style={{
        display:'table',
        marginLeft: '2%',
        height: '100%'
      }}>
          <p
            style={{
              backgroundColor: "transparent",
              fontSize: '28px',
              margin: '0%',
              color: 'white',
              lineHeight:'100%',
              display: 'table-cell',
              verticalAlign: 'middle',
              fontFamily: 'Trebuchet MS, sans-serif',
              fontWeight: 'bold'
            }}
          >
            Karta 
          </p>
        </div>
      </div>

      <div id="karta" width="600px" height="450px" style={{ height: '450px', width: '600px', marginLeft: '400px' }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "" }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
      >
        <AnyReactComponent
          lat={46.55903}
          lng={15.63786}
          text="FERI"
        />
      </GoogleMapReact>
      </div>

    </div>
  )
}
