import React, { useState } from 'react';



export default function DodajanjePostaj() {

  const [enteredText, setEnteredText] = useState(''); 
  const [enteredPosta, setEnteredPosta] = useState(''); 
  const [enteredStevilo, setEnteredStevilo] = useState(''); 
  
  const changeColor = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
  }

  //povrne barvo nazaj na prejšnje stanje
  const setToPreviousColor = event => {
    event.currentTarget.style.backgroundColor = '#E72F39';
    event.currentTarget.style.borderColor = '#E72F39';
  }


  const click = event => {
    event.currentTarget.style.backgroundColor = '#F24644';
    event.currentTarget.style.borderColor = '#F24644';
    setEnteredText('');
    setEnteredStevilo('');
    setEnteredPosta('');
  }

  const handle = ({target:{value}}) => setEnteredText(value)
  const handlePosta = ({target:{value}}) => setEnteredPosta(value)
  const handleStevilo = ({target:{value}}) => setEnteredStevilo(value)
  
  return (
    
    <div 
      style={{
        backgroundColor: 'white',
        height: '800px'
      }}>
      <div
        style={{
          backgroundColor: 'white',
          width: '10%',
          height: '10%',
          float: 'left',
          marginLeft: '2%',
          marginTop: '2%',
          marginRight: '2%',
          display: 'inline-block'
        }}>
        <img id = "logo" src="https://www.mbajk.si/assets/img/logo-contract.png" alt="logo"
        style={{
          margin: 0,
          position: 'relative',
          top: '50%',
          left: '50%',
          height: '100%',
          transform: 'translate(-50%, -50%)'
        }}
        ></img>
      </div>
      <div 
      style={{
        backgroundColor: '#E72F39',
        display:'inline-block',
        marginTop: '2%',
        height: '10%',
        width: '84%',
        borderRadius: '100px'
      }}>
        <div 
        style={{
        display:'table',
        marginLeft: '2%',
        height: '100%'
      }}>
          <p
            style={{
              backgroundColor: "transparent",
              fontSize: '28px',
              margin: '0%',
              color: 'white',
              lineHeight:'100%',
              display: 'table-cell',
              verticalAlign: 'middle',
              fontFamily: 'Trebuchet MS, sans-serif',
              fontWeight: 'bold'
            }}
          >
            Dodaj novo postajo
          </p>
        </div>
      </div>
      <div 
      style={{
        display:'inline-block',
        marginLeft: '2%',
        marginTop: '2%',
        backgroundColor: 'white',
        height: '76%',
        width: '96%'
      }}>
      <h3
      style={{
        marginLeft: '3%',
        marginTop: '2%',
        color: '#2D2D2C',
        fontSize: '22px',
        fontFamily: 'Trebuchet MS, sans-serif',
        fontWeight: 'bold'
      }}>Naslov</h3>
      <input id = "inputIme" type="text" value={enteredText} onChange={handle}
        style={{
          marginLeft: '3%',
          color: '#2D2D2C',
          fontSize: '20px',
          padding: '0.4%',
          width:'30%',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold',
          borderRadius: '5px',
          borderStyle:'solid',
          borderColor: '#707070',
          borderWidth: '1.5px'
        }}
      />
      <h3
        style={{
          marginLeft: '3%',
          marginTop: '2%',
          color: '#2D2D2C',
          fontSize: '22px',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold'
        }}
      >Poštna številka</h3>
      <input type="text" value={enteredPosta} onChange={handlePosta}
        style={{
          marginLeft: '3%',
          color: '#2D2D2C',
          fontSize: '20px',
          padding: '0.4%',
          width:'30%',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold',
          borderRadius: '5px',
          borderStyle:'solid',
          borderColor: '#707070',
          borderWidth: '1.5px'
        }}
      />
      <h3
        style={{
          marginLeft: '3%',
          marginTop: '2%',
          color: '#2D2D2C',
          fontSize: '22px',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold'
        }}
      >Število mest</h3>
      <input type="text" value={enteredStevilo} onChange={handleStevilo}
        style={{
          marginLeft: '3%',
          color: '#2D2D2C',
          fontSize: '20px',
          padding: '0.4%',
          width:'30%',
          fontFamily: 'Trebuchet MS, sans-serif',
          fontWeight: 'bold',
          borderRadius: '5px',
          borderStyle:'solid',
          borderColor: '#707070',
          borderWidth: '1.5px'
        }}
      />
      <br/>
      <button id="btn"
        style={{
            marginLeft: '3%',
            marginTop: '2%',
            fontSize: '20px',
            padding: '0.4%',
            width: '31%',
            backgroundColor: '#E72F39',
            color: "white",
            borderRadius: '10px',
            borderColor: '#E72F39',
            borderStyle:'solid'
          }}
          onMouseEnter={changeColor}
          onMouseLeave={setToPreviousColor}
          onMouseDown={setToPreviousColor}
          onMouseUp={changeColor}
          onMouseUpCapture={click}
          >
        Dodaj
      </button>
      </div>
    </div>
  )
}
